$(document).ready(function() {
	$('.slider').slick({
  		fade: true,
  		dots: true,
  		autoplay: true,
  		arrows: true
	});

	$('.slide').slick({
  		fade: true,
  		autoplay: true,
  		arrows: true
	});

	$("ul.nav-tabs a").click(function (e) {
	  e.preventDefault();  
	    $(this).tab('show');
	});

	$('.offcanvas-toggle').on('click', function() {
	  $('body').toggleClass('offcanvas-expanded');
	});



});


