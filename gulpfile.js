var config   = require('./gulpconfig.json'),
    tasks    = { js:[], less:[] };

var WP       = require('wp-cli'),
    inquirer = require('inquirer'),
    argv     = require('yargs').default('compress', 'true').argv,
    compress = (argv.compress != 'false');

var gulp     = require('gulp'),
    gulpif   = require('gulp-if'),
    plumber  = require('gulp-plumber'),
    gutil    = require('gulp-util'),
    notify   = require("gulp-notify"),
    rename   = require('gulp-rename'),
    replace  = require('gulp-replace'),
    less     = require('gulp-less'),
    include  = require('gulp-include'),
    uglify   = require('gulp-uglify');

var LessPluginCleanCSS    = require('less-plugin-clean-css'),
    LessPluginAutoPrefix  = require('less-plugin-autoprefix'),
    CombineMediaQueries   = require('less-plugin-group-css-media-queries'),
    CleanCSS              = new LessPluginCleanCSS({ advanced: false }),
    Autoprefix            = new LessPluginAutoPrefix({ browsers: ['> 1%', 'last 2 versions', 'IE >= 8', 'Firefox ESR', 'Opera 12.1'] }),
    LessPlugins           = (compress)? [CombineMediaQueries, CleanCSS, Autoprefix] : [Autoprefix];

if (!compress)
    gutil.log(gutil.colors.red('Compression is turned OFF.'));

for (var group in config.build.less)
    tasks.less.push('build:less:' + group);

for (var group in config.build.js)
    tasks.js.push('build:js:' + group);

tasks.less.forEach(function(task){
    gulp.task(task, function(done){
        var group = task.split(':').pop();
        for (var src in config.build.less[group]) {
            var pipeline = gulp.src(src)
                .pipe(plumber({
                    errorHandler: notify.onError({
                        title: 'Error running ' + task,
                        message: "<%= error.message %>"
                    })}
                ))
                .pipe(less({ plugins: LessPlugins }))
                .pipe(rename(src.split('/').pop().replace('.less', '.min.css')))
            ;
            config.build.less[group][src].forEach(function(path){
                pipeline.pipe(gulp.dest(path))
            })
            return pipeline;
        }
        done();
    });
});
gulp.task('build:less', gulp.parallel(tasks.less));

tasks.js.forEach(function(task){
    gulp.task(task, function(done){
        var group = task.split(':').pop();
        for (var src in config.build.js[group]) {
            var pipeline = gulp.src(src)
                .pipe(plumber({
                    errorHandler: notify.onError({
                        title: 'Error running ' + task,
                        message: "<%= error.message %> in line no. <%= error.lineNumber %>"
                    })}
                ))
                .pipe(include())
                .pipe(gulpif(compress, uglify()))
                .pipe(rename(src.split('/').pop().replace('.js', '.min.js')))
            ;
            config.build.js[group][src].forEach(function(path){
                pipeline.pipe(gulp.dest(path))
            })
            return pipeline;
        }
        done();
    });
});
gulp.task('build:js', gulp.parallel(tasks.js));

function wordpress(done){
    WP.discover({ path: '.' }, function(WP){
        gutil.log(gutil.colors.cyan('Downloading latest WordPress version...'));
        WP.core.download(function(error, result){
            if (error) {
                gutil.log(gutil.colors.red(error));
                done();
            } else {
                gutil.log(gutil.colors.cyan('Success: WordPress downloaded.'));
                inquirer.prompt([
                    {
                        type: 'input',
                        name: 'DB_NAME',
                        message: 'DB_NAME:',
                        validate: function(input){
                            return (!input)? 'DB_NAME is required.' : true ;
                        }
                    },
                    {
                        type: 'input',
                        name: 'DB_USER',
                        message: 'DB_USER:',
                        default: 'root'
                    },
                    {
                        type: 'input',
                        name: 'DB_PASSWORD',
                        message: 'DB_PASSWORD:',
                        default: 'root'
                    },
                    {
                        type: 'input',
                        name: 'DB_HOST',
                        message: 'DB_HOST:',
                        default: 'localhost'
                    }
                ], function(obj){
                    gulp.src('wp-config-sample.php')
                        .pipe(replace('database_name_here', obj.DB_NAME))
                        .pipe(replace('username_here', obj.DB_USER))
                        .pipe(replace('password_here', obj.DB_PASSWORD))
                        .pipe(replace('localhost', obj.DB_HOST))
                        .pipe(replace('false', true))
                        .pipe(rename('wp-config.php'))
                        .pipe(gulp.dest('.'))
                    ;
                    gutil.log(gutil.colors.cyan('Success: Generated wp-config.php file.'))
                    done();
                });
            }
        });
    });
}

function watch(){
    config.watch.forEach(function(obj){
        for (var group in obj.build) {
            gutil.log(gutil.colors.cyan('Watching ' + obj.files));
            gulp.watch(obj.files, gulp.series(
                'build:' + group + ':' + obj.build[group]
            ));
        }
    });
}

gulp.task('build', gulp.parallel(['build:less', 'build:js']));
gulp.task('build:all', gulp.parallel(['build:less', 'build:js']));
gulp.task('wp:init', wordpress);
gulp.task('watch', watch);
gulp.task('init', gulp.series(['wp:init', 'build:less', 'build:js']));
